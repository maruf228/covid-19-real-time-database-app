package com.maruf.covid19.ui.country;

public class CovidCountry {
    String cCountry, cCases, cTodayCases, cDeaths, cTodayDeaths, cRecovered, cActive, cCritical, cTotalTests;

    public CovidCountry(String cCountry, String cCases, String cTodayCases, String cDeaths, String cTodayDeaths, String cRecovered, String cActive, String cCritical, String cTotalTests) {
        this.cCountry = cCountry;
        this.cCases = cCases;
        this.cTodayCases = cTodayCases;
        this.cDeaths = cDeaths;
        this.cTodayDeaths = cTodayDeaths;
        this.cRecovered = cRecovered;
        this.cActive = cActive;
        this.cCritical = cCritical;
        this.cTotalTests = cTotalTests;
    }

    public String getcCountry() {
        return cCountry;
    }

    public void setcCountry(String cCountry) {
        this.cCountry = cCountry;
    }

    public String getcCases() {
        return cCases;
    }

    public void setcCases(String cCases) {
        this.cCases = cCases;
    }

    public String getcTodayCases() {
        return cTodayCases;
    }

    public void setcTodayCases(String cTodayCases) {
        this.cTodayCases = cTodayCases;
    }

    public String getcDeaths() {
        return cDeaths;
    }

    public void setcDeaths(String cDeaths) {
        this.cDeaths = cDeaths;
    }

    public String getcTodayDeaths() {
        return cTodayDeaths;
    }

    public void setcTodayDeaths(String cTodayDeaths) {
        this.cTodayDeaths = cTodayDeaths;
    }

    public String getcRecovered() {
        return cRecovered;
    }

    public void setcRecovered(String cRecovered) {
        this.cRecovered = cRecovered;
    }

    public String getcActive() {
        return cActive;
    }

    public void setcActive(String cActive) {
        this.cActive = cActive;
    }

    public String getcCritical() {
        return cCritical;
    }

    public void setcCritical(String cCritical) {
        this.cCritical = cCritical;
    }

    public String getcTotalTests() {
        return cTotalTests;
    }

    public void setcTotalTests(String cTotalTests) {
        this.cTotalTests = cTotalTests;
    }
}
