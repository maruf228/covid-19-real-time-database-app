package com.maruf.covid19.ui.country;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.maruf.covid19.R;

import java.util.ArrayList;

public class CovidCountryAdapter extends RecyclerView.Adapter<CovidCountryAdapter.ViewHolder> {
    ArrayList<CovidCountry> covidCountries;

    public CovidCountryAdapter(ArrayList<CovidCountry> covidCountries) {
        this.covidCountries = covidCountries;
    }
    @NonNull
    @Override
    public CovidCountryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_country, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CovidCountry covidCountry = covidCountries.get(position);

        holder.tvCountry.setText(covidCountry.getcCountry());
        holder.tvTotalCase.setText(covidCountry.getcCases());
        holder.tvTodayCases.setText(covidCountry.getcTodayCases());
        holder.tvTotalDeaths.setText(covidCountry.getcDeaths());
        holder.tvTodayDeaths.setText(covidCountry.getcTodayDeaths());
        holder.tvRecovered.setText(covidCountry.getcRecovered());
    }

    @Override
    public int getItemCount() {
        return covidCountries.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTotalCase, tvCountry, tvTodayCases, tvTotalDeaths, tvTodayDeaths, tvRecovered;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvCountry = itemView.findViewById(R.id.tvCountryName);
            tvTotalCase = itemView.findViewById(R.id.tvTotalCases);
            tvTodayCases = itemView.findViewById(R.id.tvTodayCases);
            tvTotalDeaths = itemView.findViewById(R.id.tvDeaths);
            tvTodayDeaths = itemView.findViewById(R.id.tvTodayDeaths);
            tvRecovered = itemView.findViewById(R.id.tvRecovered);
        }
    }
}
